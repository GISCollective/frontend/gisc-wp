(function (blocks, element, blockEditor) {
  var MediaUpload = blockEditor.MediaUpload;

  var el = element.createElement;
  var InnerBlocks = blockEditor.InnerBlocks;
  var useBlockProps = blockEditor.useBlockProps;

  function getViewMode(props, content) {
    var attributes = props.attributes;

    return el('div', { className: "parallax-bg-container" },
      [
        el('div', { className: "parallax-bg parallax-bg-image", style: { "background-image": "url(" + attributes["layerURL1"] + ")" } }, ""),
        el('div', { className: "parallax-bg parallax-bg-image", style: { "background-image": "url(" + attributes["layerURL2"] + ")" } }, ""),
        el('div', { className: "parallax-bg parallax-bg-image", style: { "background-image": "url(" + attributes["layerURL3"] + ")" } }, ""),
        el('div', { className: "parallax-bg parallax-bg-content" }, el( content )),
        el('div', { className: "parallax-bg parallax-bg-image", style: { "background-image": "url(" + attributes["layerURL4"] + ")" } }, "")
      ]);
  }

  blocks.registerBlockType('giscollective/parallax', {
    apiVersion: 2,
    title: 'Parallax',
    icon: 'universal-access-alt',
    category: 'design',
    example: {},
    attributes: {
      layerId1: { type: 'number', default: 0 },
      layerURL1: { type: 'string', default: '' },

      layerId2: { type: 'number', default: 0 },
      layerURL2: { type: 'string', default: '' },

      layerId3: { type: 'number', default: 0 },
      layerURL3: { type: 'string', default: '' },

      layerId4: { type: 'number', default: 0 },
      layerURL4: { type: 'string', default: '' },
    },
    edit: function (props) {
      var blockProps = useBlockProps();
      var attributes = props.attributes;

      function getUploadProperties(index) {
        let idKey = "layerId" + index;
        let urlKey = "layerURL" + index;

        var onSelectImage = function (media) {
          let newAttributes = {};
          newAttributes[urlKey] = media.url;
          newAttributes[idKey] = media.id;

          return props.setAttributes(newAttributes);
        };

        return {
          onSelect: onSelectImage,
          type: 'image',
          value: attributes[idKey],
          render: function renderer(obj) {
            return el( 'button', { className: "btn btn-parallax-select-layer btn-parallax-select-layer-" + index, onClick: obj.open }, "Select Layer " + index )
          }
        }
      }

      return el(
        'div',
        blockProps,
        getViewMode(props, InnerBlocks),
        el('div', { className: "btn-layer-group" }, [
          el(MediaUpload, getUploadProperties(1)),
          el(MediaUpload, getUploadProperties(2)),
          el(MediaUpload, getUploadProperties(3)),
          el(MediaUpload, getUploadProperties(4)),
        ]),
        el(
          'div',
          el(InnerBlocks.Content)
        ),
      );
    },
    save: function (props) {
      var blockProps = useBlockProps.save();

      return (
        el('div', blockProps, getViewMode(props, InnerBlocks.Content))
      );
    },
  });
}(
  window.wp.blocks,
  window.wp.element,
  window.wp.blockEditor,
));
