function readyFn( ) {
  let $ = jQuery;
  let clock = 0;
  let currentClock = -1;


  function repeatOften() {
    if(clock <= currentClock) {
      return;
    }

    currentClock = clock;
    updateParallax();
  }

  let scrollWatch = requestAnimationFrame(repeatOften);

  function updateParallax() {
    const top = $(".wp-block-giscollective-parallax")[0].getBoundingClientRect().bottom;
    const h = $(".wp-block-giscollective-parallax").height();

    const diff = Math.max(h - top, 0);

    const bg = $(".parallax-bg-image");

    bg.each(function(index) {
      $(this).css("transform", "translate3d(0, " + diff / (index + 1) + "px, 0)");
    });

    $(".parallax-bg-content").css({
      "margin-top": (diff * 1.2) + "px",
      "opacity": 1 - (diff / h)
    });
  }

  setTimeout(() => {
    $(window).resize();
  }, 500);

  $(window).on("resize", function() {
    clock++;
    scrollWatch = requestAnimationFrame(repeatOften);
  }).resize();

  $(window).on("scroll", function() {
    clock++;
    scrollWatch = requestAnimationFrame(repeatOften);
  });
}

jQuery(document).ready( readyFn );