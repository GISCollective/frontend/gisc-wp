<?php

/**
 * Plugin Name: Parallax
 * Plugin URI: https://gitlab.com/GISCollective/frontend/gisc-wp
 * Description: This is a plugin that shows a parallax image block
 * Version: 1.0.0
 * Author: GISCollective
 *
 * @package GISCollective
 */

defined( 'ABSPATH' ) || exit;

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 *
 * Passes translations to JavaScript.
 */
function giscollective_parallax_register_block() {
  if (!function_exists( 'register_block_type' ) ) {
    // Gutenberg is not active.
    return;
  }

  wp_register_script(
    'parallax-block',
    plugins_url( 'block.js', __FILE__ ),
    array( 'wp-blocks', 'wp-element', 'wp-block-editor' ),
    filemtime( plugin_dir_path( __FILE__ ) . 'block.js' )
  );

  wp_register_script(
    'parallax',
    plugins_url( 'block-view.js', __FILE__ ),
    array( 'jquery' ),
    filemtime( plugin_dir_path( __FILE__ ) . 'block-view.js' )
  );

  wp_register_style(
      'parallax-editor',
      plugins_url( 'editor.css', __FILE__ ),
      array( 'wp-edit-blocks' ),
      filemtime( plugin_dir_path( __FILE__ ) . 'editor.css' )
  );

  wp_register_style(
      'parallax',
      plugins_url( 'style.css', __FILE__ ),
      array( ),
      filemtime( plugin_dir_path( __FILE__ ) . 'style.css' )
  );

  register_block_type( 'giscollective/parallax', array(
    'apiVersion' => 2,
    'style' => 'parallax',
    'script' => 'parallax',
    'editor_style' => 'parallax',
    'editor_script' => 'parallax-block',
  ));
}

add_action( 'init', 'giscollective_parallax_register_block' );
